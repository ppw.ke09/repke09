Nama-nama Anggota Kelompok :
- Azna Ajeng Nurfaiza
- Jacob Marulitua Ido Situmorang
- Kevin Asyraf Putra Priyatna
- Zuhrah Annafira

Link Herokuapp :
ppwke09.herokuapp.com

Cerita Aplikasi :
Kami membuat stock bot, dalam hal ini berbentuk web trading saham yang memiliki fitur automasi seperti auto cut loss, yaitu ketika harga saham yang kita punya sudah turun x% dengan x berdasarkan dari keinginan user, maka web ini akan langsung menjual saham tersebut dengan tujuan agar mencegah harga saham turun sekali sehingga memperbesar kerugian atau yang memiliki istilah "saham nyangkut"

Daftar fitur :
- Sign Up
- Login
- Search data saham yang tersedia
- Pembelian saham dan penjualan saham yang dimiliki
- Pemilihan angka x% dengan x adalah angka parameter auto cut loss yang diinginkan user