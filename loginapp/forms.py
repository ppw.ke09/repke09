from django import forms
from .models import Snippet


class ContactForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()

def hasil_detail(request):
    hasil = Snippet.objects.all()
    return render(request, 'test.html', {'hasil': hasil})
    
class SnippetForm(forms.ModelForm):
    class Meta:
        model = Snippet
        widgets = {
            'password': forms.PasswordInput(),
        }
        fields = ('username', 'password')