from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import ContactForm,SnippetForm
from .models import Snippet


# Create your views here.

def contact(request):
    form = ContactForm()
    return render(request, 'loginhtml.html', {'form':form})

def index(request):
    return render(request, 'test.html')

def hasil_detail(request):
    hasil = Snippet.objects.all()
    return render(request, 'test.html', {'hasil': hasil})
