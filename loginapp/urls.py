from django.urls import path
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', views.contact),
    path('login/', LoginView.as_view(template_name='loginhtml.html'), name="login"),
    path('test/', views.hasil_detail),

]
