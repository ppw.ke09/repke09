from django.test import TestCase, Client

# Create your tests here.

class welcomeTest(TestCase):
    def test_URL(self):
        c = Client()
        response = c.get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_input(self):
        c = Client()
        response = c.get('/login/')
        content = response.content.decode('utf8') #jd hurufnya universal, nerjemahin gt
        self.assertIn ("<input", content)
    
    def test_button(self):
        c = Client()
        response = c.get('/login/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Login", content)